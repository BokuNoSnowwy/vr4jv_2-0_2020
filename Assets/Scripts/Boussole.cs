﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boussole : MonoBehaviour
{
    public List <GameObject> targets;
    GameObject currentTarget;
    // Start is called before the first frame update
    void Start()
    {
        currentTarget = targets[0];
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject target in targets)
        {
            if(Mathf.Abs(Vector3.Distance(transform.position,target.transform.position))<Mathf.Abs(Vector3.Distance(transform.position,currentTarget.transform.position))){
                currentTarget = target;
            }
        }
        transform.LookAt(currentTarget.transform);
        transform.rotation = new Quaternion (0,transform.rotation.y,0,transform.rotation.w);
    }
}
